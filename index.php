<?php

  require_once(__DIR__.'/vendor/autoload.php');
  use LightnCandy\LightnCandy;

  $context_array = array(
    'name' => 'Milo', 
    'value' => 100,
  );

  // Add repeater items to main data array
  $context_array['items'] = array(
    'slide1' => array(
      'title' => "Slide #1",
      'description' => "This is the first slide.",
      'url' => "http://google.com/search?q=slide1"
    ),
    'slide2' => array(
      'title' => "Slide #2",
      'description' => "This is the second slide.",
      'url' => "http://google.com/search?q=slide2"
    ),
    'slide3' => array(
      'title' => "Slide #3",
      'description' => "This is the third slide.",
      'url' => "http://google.com/search?q=slide3"
    ),
  );

  $template = file_get_contents( __DIR__ . '/template.html' );

  $phpStr = LightnCandy::compile($template, array('flags' => LightnCandy::FLAG_HANDLEBARSJS,
    'helpers' => Array(
      'list' => function ($context, $options) {
        $out = '';
        $data = $options['data'];

        foreach ($context as $idx => $cx) {
          $data['index'] = $idx;
          $out .= $options['fn']($cx, Array('data' => $data));
        }
        return $out;
      },
      'ifvalue' => function ($conditional, $options) {
        if ($conditional == $options['hash']['equals']) {
          return $options['fn']();
        } else {
          return $options['inverse']();
        }
      }
    )
  ));
  $renderer = LightnCandy::prepare($phpStr);

  $output = $renderer($context_array);

  echo $output;
 ?>

